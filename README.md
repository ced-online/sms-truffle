# Student Management System with truffle



## Setting up the development environment 

Before we start we need some tools and dependencies. Please install the following:

1. Node.js and npm (comes with Node)
2. Git
3. Geth
4. Truffle
5. ganache-cli

## Running the geth client or ganache-cli
$ geth --dev --networkid 4002 --rpc --rpcport 8545 --rpccorsdomain "*" --rpcapi " personal,
db, eth, net, web3"

$ ganache-cli -i 4002



## Compiling and deploying the smart contract.

1. Compile the contract using following command  <br/>
    $ truffle compile
2. Deploy the contract using following command  <br/>
    $ truffle migrate 


## Edit app.js

 Replace account address with your geth account address or ganache address


## Installation
Install all dependencies using following command <br />

$ npm install

## Run Dapp
Using following command, we can run the application<br />
$ npm start

