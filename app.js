var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var Web3 = require("web3");
var MyContractJson = require(path.join(__dirname,'truffleDev/build/contracts/StudentManagementSystem.json'))

web3 = new Web3("http://localhost:8545");

coinbase = "0x34b9F6C254A83cD261790755B8a87f7457551e81";
var contractAddress = MyContractJson.networks['5777'].address;
var contractAbi = MyContractJson.abi;

SMS = new web3.eth.Contract(contractAbi, contractAddress);

var indexRouter = require('./routes/index');
var setStudentRouter = require('./routes/setStudent');
var getStudentRouter = require('./routes/getStudent');
var feePaymentRouter = require('./routes/feePayment');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/setStudent', setStudentRouter);
app.use('/getStudent', getStudentRouter);
app.use('/feePayment', feePaymentRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
