var express = require('express');
var router = express.Router();

router.post('/', function (req, res, next) {
    data = req.body;
    console.log(data);
    SMS.methods.feePayment(data.roll).send({ from: data.payer, value: data.amount, gas: 600000 });
    res.send("Fees Paid");
});

module.exports = router;
